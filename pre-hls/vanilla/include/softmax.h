/* softmax.h
 * 
 * authors: Rafael COSTA SALES
 *          Duc Huy DAO
 */

#include <algorithm>
#include <math.h>
#include <numeric>
#include <iostream>

float softmax(float *input);
