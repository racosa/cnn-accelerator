/* fc.h
 * 
 * authors: Rafael COSTA SALES
 *          Duc Huy DAO
 */

void reshape(float input[], float output[]);
void fully_connected(float input[], const float matrix[], float output[], const float bias[]);
