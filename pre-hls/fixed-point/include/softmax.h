/* softmax.h
 * 
 * authors: Rafael COSTA SALES
 *          Duc Huy DAO
 */

#include <algorithm>
#include <math.h>
#include <numeric>
#include <iostream>
#include "../lib/ac_fixed.h"

float softmax(double *input);
